﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.MarchingCubes
{
	public delegate void CubeComputation(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z);

	public class MarchingCubesFunctions
	{
		public readonly CubeComputation[] Configurations = new CubeComputation[256];

		public MarchingCubesFunctions()
		{
			Configurations[0] = Compute00000000;
			Configurations[1] = Compute00000001;
			Configurations[2] = Compute00000010;
			Configurations[3] = Compute00000011;
			Configurations[4] = Compute00000100;
			Configurations[5] = Compute00000101;
			Configurations[6] = Compute00000110;
			Configurations[7] = Compute00000111;
			Configurations[8] = Compute00001000;
			Configurations[9] = Compute00001001;
			Configurations[10] = Compute00001010;
			Configurations[11] = Compute00001011;
			Configurations[12] = Compute00001100;
			Configurations[13] = Compute00001101;
			Configurations[14] = Compute00001110;
			Configurations[15] = Compute00001111;
			Configurations[16] = Compute00010000;
			Configurations[17] = Compute00010001;
			Configurations[18] = Compute00010010;
			Configurations[19] = Compute00010011;
			Configurations[20] = Compute00010100;
			Configurations[21] = Compute00010101;
			Configurations[22] = Compute00010110;
			Configurations[23] = Compute00010111;
			Configurations[24] = Compute00011000;
			Configurations[25] = Compute00011001;
			Configurations[26] = Compute00011010;
			Configurations[27] = Compute00011011;
			Configurations[28] = Compute00011100;
			Configurations[29] = Compute00011101;
			Configurations[30] = Compute00011110;
			Configurations[31] = Compute00011111;
			Configurations[32] = Compute00100000;
			Configurations[33] = Compute00100001;
			Configurations[34] = Compute00100010;
			Configurations[35] = Compute00100011;
			Configurations[36] = Compute00100100;
			Configurations[37] = Compute00100101;
			Configurations[38] = Compute00100110;
			Configurations[39] = Compute00100111;
			Configurations[40] = Compute00101000;
			Configurations[41] = Compute00101001;
			Configurations[42] = Compute00101010;
			Configurations[43] = Compute00101011;
			Configurations[44] = Compute00101100;
			Configurations[45] = Compute00101101;
			Configurations[46] = Compute00101110;
			Configurations[47] = Compute00101111;
			Configurations[48] = Compute00110000;
			Configurations[49] = Compute00110001;
			Configurations[50] = Compute00110010;
			Configurations[51] = Compute00110011;
			Configurations[52] = Compute00110100;
			Configurations[53] = Compute00110101;
			Configurations[54] = Compute00110110;
			Configurations[55] = Compute00110111;
			Configurations[56] = Compute00111000;
			Configurations[57] = Compute00111001;
			Configurations[58] = Compute00111010;
			Configurations[59] = Compute00111011;
			Configurations[60] = Compute00111100;
			Configurations[61] = Compute00111101;
			Configurations[62] = Compute00111110;
			Configurations[63] = Compute00111111;
			Configurations[64] = Compute01000000;
			Configurations[65] = Compute01000001;
			Configurations[66] = Compute01000010;
			Configurations[67] = Compute01000011;
			Configurations[68] = Compute01000100;
			Configurations[69] = Compute01000101;
			Configurations[70] = Compute01000110;
			Configurations[71] = Compute01000111;
			Configurations[72] = Compute01001000;
			Configurations[73] = Compute01001001;
			Configurations[74] = Compute01001010;
			Configurations[75] = Compute01001011;
			Configurations[76] = Compute01001100;
			Configurations[77] = Compute01001101;
			Configurations[78] = Compute01001110;
			Configurations[79] = Compute01001111;
			Configurations[80] = Compute01010000;
			Configurations[81] = Compute01010001;
			Configurations[82] = Compute01010010;
			Configurations[83] = Compute01010011;
			Configurations[84] = Compute01010100;
			Configurations[85] = Compute01010101;
			Configurations[86] = Compute01010110;
			Configurations[87] = Compute01010111;
			Configurations[88] = Compute01011000;
			Configurations[89] = Compute01011001;
			Configurations[90] = Compute01011010;
			Configurations[91] = Compute01011011;
			Configurations[92] = Compute01011100;
			Configurations[93] = Compute01011101;
			Configurations[94] = Compute01011110;
			Configurations[95] = Compute01011111;
			Configurations[96] = Compute01100000;
			Configurations[97] = Compute01100001;
			Configurations[98] = Compute01100010;
			Configurations[99] = Compute01100011;
			Configurations[100] = Compute01100100;
			Configurations[101] = Compute01100101;
			Configurations[102] = Compute01100110;
			Configurations[103] = Compute01100111;
			Configurations[104] = Compute01101000;
			Configurations[105] = Compute01101001;
			Configurations[106] = Compute01101010;
			Configurations[107] = Compute01101011;
			Configurations[108] = Compute01101100;
			Configurations[109] = Compute01101101;
			Configurations[110] = Compute01101110;
			Configurations[111] = Compute01101111;
			Configurations[112] = Compute01110000;
			Configurations[113] = Compute01110001;
			Configurations[114] = Compute01110010;
			Configurations[115] = Compute01110011;
			Configurations[116] = Compute01110100;
			Configurations[117] = Compute01110101;
			Configurations[118] = Compute01110110;
			Configurations[119] = Compute01110111;
			Configurations[120] = Compute01111000;
			Configurations[121] = Compute01111001;
			Configurations[122] = Compute01111010;
			Configurations[123] = Compute01111011;
			Configurations[124] = Compute01111100;
			Configurations[125] = Compute01111101;
			Configurations[126] = Compute01111110;
			Configurations[127] = Compute01111111;
			Configurations[128] = Compute10000000;
			Configurations[129] = Compute10000001;
			Configurations[130] = Compute10000010;
			Configurations[131] = Compute10000011;
			Configurations[132] = Compute10000100;
			Configurations[133] = Compute10000101;
			Configurations[134] = Compute10000110;
			Configurations[135] = Compute10000111;
			Configurations[136] = Compute10001000;
			Configurations[137] = Compute10001001;
			Configurations[138] = Compute10001010;
			Configurations[139] = Compute10001011;
			Configurations[140] = Compute10001100;
			Configurations[141] = Compute10001101;
			Configurations[142] = Compute10001110;
			Configurations[143] = Compute10001111;
			Configurations[144] = Compute10010000;
			Configurations[145] = Compute10010001;
			Configurations[146] = Compute10010010;
			Configurations[147] = Compute10010011;
			Configurations[148] = Compute10010100;
			Configurations[149] = Compute10010101;
			Configurations[150] = Compute10010110;
			Configurations[151] = Compute10010111;
			Configurations[152] = Compute10011000;
			Configurations[153] = Compute10011001;
			Configurations[154] = Compute10011010;
			Configurations[155] = Compute10011011;
			Configurations[156] = Compute10011100;
			Configurations[157] = Compute10011101;
			Configurations[158] = Compute10011110;
			Configurations[159] = Compute10011111;
			Configurations[160] = Compute10100000;
			Configurations[161] = Compute10100001;
			Configurations[162] = Compute10100010;
			Configurations[163] = Compute10100011;
			Configurations[164] = Compute10100100;
			Configurations[165] = Compute10100101;
			Configurations[166] = Compute10100110;
			Configurations[167] = Compute10100111;
			Configurations[168] = Compute10101000;
			Configurations[169] = Compute10101001;
			Configurations[170] = Compute10101010;
			Configurations[171] = Compute10101011;
			Configurations[172] = Compute10101100;
			Configurations[173] = Compute10101101;
			Configurations[174] = Compute10101110;
			Configurations[175] = Compute10101111;
			Configurations[176] = Compute10110000;
			Configurations[177] = Compute10110001;
			Configurations[178] = Compute10110010;
			Configurations[179] = Compute10110011;
			Configurations[180] = Compute10110100;
			Configurations[181] = Compute10110101;
			Configurations[182] = Compute10110110;
			Configurations[183] = Compute10110111;
			Configurations[184] = Compute10111000;
			Configurations[185] = Compute10111001;
			Configurations[186] = Compute10111010;
			Configurations[187] = Compute10111011;
			Configurations[188] = Compute10111100;
			Configurations[189] = Compute10111101;
			Configurations[190] = Compute10111110;
			Configurations[191] = Compute10111111;
			Configurations[192] = Compute11000000;
			Configurations[193] = Compute11000001;
			Configurations[194] = Compute11000010;
			Configurations[195] = Compute11000011;
			Configurations[196] = Compute11000100;
			Configurations[197] = Compute11000101;
			Configurations[198] = Compute11000110;
			Configurations[199] = Compute11000111;
			Configurations[200] = Compute11001000;
			Configurations[201] = Compute11001001;
			Configurations[202] = Compute11001010;
			Configurations[203] = Compute11001011;
			Configurations[204] = Compute11001100;
			Configurations[205] = Compute11001101;
			Configurations[206] = Compute11001110;
			Configurations[207] = Compute11001111;
			Configurations[208] = Compute11010000;
			Configurations[209] = Compute11010001;
			Configurations[210] = Compute11010010;
			Configurations[211] = Compute11010011;
			Configurations[212] = Compute11010100;
			Configurations[213] = Compute11010101;
			Configurations[214] = Compute11010110;
			Configurations[215] = Compute11010111;
			Configurations[216] = Compute11011000;
			Configurations[217] = Compute11011001;
			Configurations[218] = Compute11011010;
			Configurations[219] = Compute11011011;
			Configurations[220] = Compute11011100;
			Configurations[221] = Compute11011101;
			Configurations[222] = Compute11011110;
			Configurations[223] = Compute11011111;
			Configurations[224] = Compute11100000;
			Configurations[225] = Compute11100001;
			Configurations[226] = Compute11100010;
			Configurations[227] = Compute11100011;
			Configurations[228] = Compute11100100;
			Configurations[229] = Compute11100101;
			Configurations[230] = Compute11100110;
			Configurations[231] = Compute11100111;
			Configurations[232] = Compute11101000;
			Configurations[233] = Compute11101001;
			Configurations[234] = Compute11101010;
			Configurations[235] = Compute11101011;
			Configurations[236] = Compute11101100;
			Configurations[237] = Compute11101101;
			Configurations[238] = Compute11101110;
			Configurations[239] = Compute11101111;
			Configurations[240] = Compute11110000;
			Configurations[241] = Compute11110001;
			Configurations[242] = Compute11110010;
			Configurations[243] = Compute11110011;
			Configurations[244] = Compute11110100;
			Configurations[245] = Compute11110101;
			Configurations[246] = Compute11110110;
			Configurations[247] = Compute11110111;
			Configurations[248] = Compute11111000;
			Configurations[249] = Compute11111001;
			Configurations[250] = Compute11111010;
			Configurations[251] = Compute11111011;
			Configurations[252] = Compute11111100;
			Configurations[253] = Compute11111101;
			Configurations[254] = Compute11111110;
			Configurations[255] = Compute11111111;
		}

		public static void Compute00000000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
		}
		public static void Compute11111111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
		}
		public static void Compute00000001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
		}
		public static void Compute00010000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
		}
		public static void Compute10000000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
		}
		public static void Compute00001000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
		}
		public static void Compute00000010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
		}
		public static void Compute00100000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
		}
		public static void Compute01000000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
		}
		public static void Compute00000100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
		}
		public static void Compute11111110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
		}
		public static void Compute11101111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
		}
		public static void Compute01111111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
		}
		public static void Compute11110111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
		}
		public static void Compute11111101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
		}
		public static void Compute11011111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
		}
		public static void Compute10111111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
		}
		public static void Compute11111011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
		}
		public static void Compute00000011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute00110000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute11000000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute00001100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute00100010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute01100000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute01000100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute00000110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute00010001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute10010000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute10001000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute00001001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
		}
		public static void Compute11111100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute11001111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute00111111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute11110011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute11011101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute10011111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute10111011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute11111001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute11101110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute01101111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute01110111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute11110110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute00100001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute01010000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute10000100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute00001010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute00010010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute10100000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute01001000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute00000101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute10000001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute00011000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute01000010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute00100100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute11011110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute10101111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute01111011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11110101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11101101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute01011111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute10110111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11111010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute01111110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11100111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute10111101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11011011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute01000001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute00010100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute10000010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute00101000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 5); triangles.Add(zero + 4); triangles.Add(zero + 3);
		}
		public static void Compute10111110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11101011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute01111101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11010111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 5); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute00001110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute00100011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute01110000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute11000100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute01100100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute01000110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute00100110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute01100010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute00110010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute11100000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute01001100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute00000111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute00110001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute11010000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute10001100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute00001011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute10011000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute10001001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute00011001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute10010001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute00001101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute00010011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute10110000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute11001000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 0); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 0); triangles.Add(zero + 2);
		}
		public static void Compute11110001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute11011100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute10001111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute00111011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute10011011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute10111001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute11011001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute10011101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute11001101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute00011111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute10110011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute11111000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute11001110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute00101111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute01110011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute11110100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute01100111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute01110110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute11100110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute01101110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute11110010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute11101100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute01001111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute00110111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 0);
			triangles.Add(zero + 4); triangles.Add(zero + 2); triangles.Add(zero + 0);
		}
		public static void Compute01000011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute00110100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute11000010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute00101100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute10100010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute01101000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute01000101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute00010110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute01010001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute10010100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute10001010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute00101001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute10000011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute00111000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute11000001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute00011100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute01001001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute00010101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute10010010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute10101000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute10000110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute00101010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute01100001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute01010100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 1); triangles.Add(zero + 0);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 6); triangles.Add(zero + 5); triangles.Add(zero + 4);
		}
		public static void Compute10111100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11001011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute00111101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11010011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01011101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10010111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10111010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11101001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10101110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01101011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01110101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11010110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01111100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11000111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute00111110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11100011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10110110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11101010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01101101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01010111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01111001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11010101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10011110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10101011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 2); triangles.Add(zero + 0); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 6); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01010010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 5); triangles.Add(zero + 4);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
		}
		public static void Compute10100100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 5); triangles.Add(zero + 4);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
		}
		public static void Compute01001010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 5); triangles.Add(zero + 4);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
		}
		public static void Compute00100101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 5); triangles.Add(zero + 4);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
		}
		public static void Compute10100001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 5); triangles.Add(zero + 4);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
		}
		public static void Compute01011000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 5); triangles.Add(zero + 4);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
		}
		public static void Compute10000101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 5); triangles.Add(zero + 4);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
		}
		public static void Compute00011010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 5); triangles.Add(zero + 4);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
		}
		public static void Compute10101101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 8); triangles.Add(zero + 7);
		}
		public static void Compute01011011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 8); triangles.Add(zero + 7);
		}
		public static void Compute10110101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 8); triangles.Add(zero + 7);
		}
		public static void Compute11011010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 8); triangles.Add(zero + 7);
		}
		public static void Compute01011110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 8); triangles.Add(zero + 7);
		}
		public static void Compute10100111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 8); triangles.Add(zero + 7);
		}
		public static void Compute01111010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 8); triangles.Add(zero + 7);
		}
		public static void Compute11100101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 8); triangles.Add(zero + 7);
		}
		public static void Compute00001111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute00110011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute11110000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute11001100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute01100110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute10011001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
		}
		public static void Compute10001101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
			triangles.Add(zero + 0); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute00011011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
			triangles.Add(zero + 0); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10110001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
			triangles.Add(zero + 0); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11011000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
			triangles.Add(zero + 0); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01001110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
			triangles.Add(zero + 0); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute00100111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
			triangles.Add(zero + 0); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01110010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
			triangles.Add(zero + 0); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11100100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
			triangles.Add(zero + 0); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01010101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
			triangles.Add(zero + 5); triangles.Add(zero + 7); triangles.Add(zero + 6);
		}
		public static void Compute10010110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
			triangles.Add(zero + 5); triangles.Add(zero + 7); triangles.Add(zero + 6);
		}
		public static void Compute10101010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
			triangles.Add(zero + 5); triangles.Add(zero + 7); triangles.Add(zero + 6);
		}
		public static void Compute01101001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
			triangles.Add(zero + 5); triangles.Add(zero + 7); triangles.Add(zero + 6);
		}
		public static void Compute11000011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
			triangles.Add(zero + 5); triangles.Add(zero + 7); triangles.Add(zero + 6);
		}
		public static void Compute00111100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 1); triangles.Add(zero + 3); triangles.Add(zero + 2);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
			triangles.Add(zero + 5); triangles.Add(zero + 7); triangles.Add(zero + 6);
		}
		public static void Compute01001101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute00010111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10110010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11101000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11000110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute00101110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01100011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute01110100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute11010001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10011100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute10001011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute00111001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 1); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
		}
		public static void Compute00011110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute10100011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute01111000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute11000101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute01100101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute01010110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute10100110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute01101010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute00111010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute11100001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute01011100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute10000111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute00110101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute11010010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute10101100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute01001011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute10011010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute10101001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute01011001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute10010101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute00101101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute01010011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute10110100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute11001010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 3); triangles.Add(zero + 2); triangles.Add(zero + 4);
			triangles.Add(zero + 4); triangles.Add(zero + 5); triangles.Add(zero + 6);
		}
		public static void Compute10100101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
			triangles.Add(zero + 9); triangles.Add(zero + 10); triangles.Add(zero + 11);
		}
		public static void Compute01011010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 1);
			triangles.Add(zero + 3); triangles.Add(zero + 4); triangles.Add(zero + 5);
			triangles.Add(zero + 6); triangles.Add(zero + 7); triangles.Add(zero + 8);
			triangles.Add(zero + 9); triangles.Add(zero + 10); triangles.Add(zero + 11);
		}
		public static void Compute10001110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute00101011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute01110001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11010100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute01101100(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute01000111(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute00110110(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11100010(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute10111000(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute11001001(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute00011101(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 0.5f, y + 1.0f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}
		public static void Compute10010011(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
		{
			int zero = vertices.Count;
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 0.0f));
			vertices.Add(new Vector3(x + 0.5f, y + 0.0f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 0.5f, z + 1.0f));
			vertices.Add(new Vector3(x + 0.0f, y + 1.0f, z + 0.5f));
			vertices.Add(new Vector3(x + 1.0f, y + 0.5f, z + 0.0f));
			triangles.Add(zero + 0); triangles.Add(zero + 1); triangles.Add(zero + 2);
			triangles.Add(zero + 0); triangles.Add(zero + 2); triangles.Add(zero + 3);
			triangles.Add(zero + 0); triangles.Add(zero + 3); triangles.Add(zero + 4);
		}

	}
}
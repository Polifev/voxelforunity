﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.MarchingCubes;
using Assets.Scripts.World;
using UnityEditor;
using UnityEngine;

public class MCWorld : MonoBehaviour
{
	private IScalarField _field;
			//ArrayScalarField.GetFlatField(new Vector3Int(0, 0, 0));
			//ArrayScalarField.GetSphereField(new Vector3Int(0, 0, 0), 60.0f);
	private MeshFilter _meshFilter;
	private MarchingCubesFunctions _marchingCubesFunctions = new MarchingCubesFunctions();

	[SerializeField] private bool _generateUsingHeightMap = false;
	[SerializeField] private Texture2D _heightMap;
	[SerializeField] private float _force = 10.0f;
	[SerializeField] private int _baseline = 64;


	void Start ()
	{
		_meshFilter = GetComponent<MeshFilter>();

		if (_generateUsingHeightMap)
		{
			HeightMapToScalarField converter = new HeightMapToScalarField();
			_field = converter.CreateField(transform.position, _heightMap, 128, 128, 128, _baseline, _force, 1.0f);
		}

		ComputeMarchingCubes(_field);
	}

	public void ComputeMarchingCubes(IScalarField scalarField)
	{
		Mesh mesh = _meshFilter.mesh;
		mesh.Clear();
		
		IList<int> triangles = new List<int>();
		IList<Vector3> vertices = new List<Vector3>();

		for (int i = 0; i < _field.Dimensions.x - 1; i++)
			for (int j = 0; j < _field.Dimensions.y - 1; j++)
				for (int k = 0; k < _field.Dimensions.z - 1; k++)
					ComputeCube(vertices, triangles, i, j, k);

		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();

		mesh.RecalculateNormals();
		mesh.RecalculateTangents();
		mesh.RecalculateBounds();
	}

	private void ComputeCube(IList<Vector3> vertices, IList<int> triangles, int x, int y, int z)
	{
		int index = 0;
		if (_field.ValueAt(x, y, z) < 0) index += 1;
		if (_field.ValueAt(x, y, z + 1) < 0) index += 2;
		if (_field.ValueAt(x + 1, y, z + 1) < 0) index += 4;
		if (_field.ValueAt(x + 1, y, z) < 0) index += 8;
		if (_field.ValueAt(x, y + 1, z) < 0) index += 16;
		if (_field.ValueAt(x, y + 1, z + 1) < 0) index += 32;
		if (_field.ValueAt(x + 1, y + 1, z + 1) < 0) index += 64;
		if (_field.ValueAt(x + 1, y + 1, z) < 0) index += 128;

		if (_marchingCubesFunctions.Configurations[index] != null)
		{
			_marchingCubesFunctions.Configurations[index].Invoke(vertices, triangles, x * 1, y * 1, z * 1);
		}
		else
		{
			Debug.Log("Unable to find " + index);
		}
	}
}

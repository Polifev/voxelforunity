﻿Shader "Triplanar/BasicTriplanar" {
	Properties
	{
		_DiffuseMap("Diffuse Map ", 2D) = "white" {}
		_NormalMap("Normal ", 2D) = "white" {}
		_Roughness("Roughness ", 2D) = "white" {}
		_AmbientOcclusion("Occlusion ", 2D) = "white" {}

		_TextureScale("Texture Scale",float) = 1
		_TriplanarBlendSharpness("Blend Sharpness",float) = 1
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#pragma target 3.0
			#pragma surface surf Lambert

			sampler2D _DiffuseMap;
			sampler2D _NormalMap;
			sampler2D _Roughness;
			sampler2D _AmbientOcclusion;

			float _TextureScale;
			float _TriplanarBlendSharpness;

			struct Input
			{
				float3 worldPos;
				float3 worldNormal;INTERNAL_DATA
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				float3x3 rotationMatrix = float3x3(
					0.9493970, -0.2191856, 0.2249511,
					0.2684916, 0.9380139, -0.2191856,
					-0.1629649, 0.2684916, 0.9493970);

				// Find our UVs for each axis based on world position of the fragment.
				half2 yUV = IN.worldPos.xz / _TextureScale;
				half2 xUV = IN.worldPos.zy / _TextureScale;
				half2 zUV = IN.worldPos.xy / _TextureScale;

				// Now do texture samples from our diffuse map with each of the 3 UV set's we've just made.
				half3 yDiff = tex2D(_DiffuseMap, yUV);
				half3 xDiff = tex2D(_DiffuseMap, xUV);
				half3 zDiff = tex2D(_DiffuseMap, zUV);

				// Now do texture samples from our diffuse map with each of the 3 UV set's we've just made.
				half3 yNorm = UnpackNormal(tex2D(_NormalMap, yUV));
				half3 xNorm = UnpackNormal(tex2D(_NormalMap, xUV));
				half3 zNorm = UnpackNormal(tex2D(_NormalMap, zUV));

				// Now do texture samples from our diffuse map with each of the 3 UV set's we've just made.
				/*half3 yOccl = tex2D(_AmbientOcclusion, yUV);
				half3 xOccl = tex2D(_AmbientOcclusion, xUV);
				half3 zOccl = tex2D(_AmbientOcclusion, zUV);*/

				// Get the absolute value of the world normal.
				// Put the blend weights to the power of BlendSharpness, the higher the value, 
				// the sharper the transition between the planar maps will be.
				float3 rotatedNormal = mul(rotationMatrix, IN.worldNormal);

				half3 blendWeights = pow(abs(rotatedNormal), _TriplanarBlendSharpness);

				// Divide our blend mask by the sum of it's components, this will make x+y+z=1
				blendWeights = blendWeights / (blendWeights.x + blendWeights.y + blendWeights.z);

				// Finally, blend together all three samples based on the blend mask.
				o.Albedo = xDiff * blendWeights.x + yDiff * blendWeights.y + zDiff * blendWeights.z;
				o.Alpha = 1.0;
				//o.Normal = xNorm * blendWeights.x + yNorm * blendWeights.y + zNorm * blendWeights.z;
				//o.Occlusion = xOccl * blendWeights.x + yOccl * blendWeights.y + zOccl * blendWeights.z;
			}
			ENDCG
		}
}

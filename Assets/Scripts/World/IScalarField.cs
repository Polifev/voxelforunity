﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.World
{
	public interface IScalarField
	{
		float ValueAt(int x, int y, int z);
		Vector3Int Dimensions { get; }
		Vector3 Position { get; }
	}
}

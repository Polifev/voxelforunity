﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.World
{
	public class HeightMapToScalarField
	{
		public IScalarField CreateField(Vector3 position, Texture2D texture, int width, int height, int length, int baseline, float force, float scale)
		{
			float[,,] field = new float[width,height,length];

			for(int i = 0; i < width; i++)
			for (int k = 0; k < length; k++)
			{
				Color c = texture.GetPixel((int)(i * scale) % texture.width, (int)(k * scale) % texture.height);
				float grey = (c.r + c.g + c.b) / 3;
				float value = force * grey;
				Debug.Log(value);
				for (int j = 0; j < height; j++)
					if (j < baseline + value)
					{
						field[i, j, k] = 1.0f;
					}
					else
					{
						field[i, j, k] = -1.0f;
					}
			}
			return new ArrayScalarField(field, position);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.World;
using UnityEngine;

public class ArrayScalarField : IScalarField
{
	private static readonly int DefaultWidth = 128;
	private static readonly int DefaultHeight = 128;
	private static readonly int DefaultLength = 128;

	private readonly float[,,] _field;

	public ArrayScalarField(float[,,] field, Vector3 position)
	{
		_field = field;
		Position = position;
	}

	public static ArrayScalarField GetFlatField(Vector3Int position)
	{
		float[,,] field = new float[DefaultWidth, DefaultHeight, DefaultLength];
		for (int i = 0; i < DefaultWidth; i++)
		{
			for (int j = 0; j < DefaultHeight; j++)
			{
				for (int k = 0; k < DefaultLength; k++)
				{
					field[i, j, k] = (j < DefaultHeight / 2) ? 1.0f : -1.0f;
				}
			}
		}

		return new ArrayScalarField(field, position);
	}

	public static ArrayScalarField GetSphereField(Vector3Int position, float radius)
	{
		Vector3 center = position;
		center.x += (float)DefaultWidth / 2;
		center.y += (float)DefaultHeight / 2;
		center.z += (float)DefaultLength / 2;

		float[,,] field = new float[DefaultWidth, DefaultHeight, DefaultLength];
		for (int i = 0; i < DefaultWidth; i++)
		{
			for (int j = 0; j < DefaultHeight; j++)
			{
				for (int k = 0; k < DefaultLength; k++)
				{
					float distanceToCenter = (i - center.x) * (i - center.x) + (j - center.y) * (j - center.y) + (k - center.z) * (k - center.z);
					field[i, j, k] = (distanceToCenter < radius) ? 1.0f : -1.0f;
				}
			}
		}

		return new ArrayScalarField(field, position);
	}

	public Vector3Int Dimensions
	{
		get
		{
			return new Vector3Int(_field.GetLength(0), _field.GetLength(1), _field.GetLength(2));
		}
	}

	public Vector3 Position { get; private set; }

	public float ValueAt(int x, int y, int z)
	{
		return _field[x,y,z];
	}
}
